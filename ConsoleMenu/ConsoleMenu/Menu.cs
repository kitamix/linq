﻿using System;
using System.Linq;
using ConsoleMenu.Models;

namespace ConsoleMenu
{
    public static class Menu
    {
        public static void Start()
        {
            Console.Clear();
            Console.WriteLine("---###MENU###---");
            Console.WriteLine("1. Count tasks of current user");
            Console.WriteLine("2. Get all tasks of user with name length < 45");
            Console.WriteLine("3. Get finished this year(2021) tasks of user");
            Console.WriteLine("4. Get users older than 10 sorted by registration date and grouped by team");
            Console.WriteLine("5. Sort by user first name and task name");
            Console.WriteLine("6. Analyze user's last tasks and project");
            Console.WriteLine("7. Analyze project's tasks and team");
            Console.WriteLine("8. Exit");
            Console.Write("\nWrite chosen menu option: ");

            if (!int.TryParse(Console.ReadLine(), out var menuKey))
            {
                Console.WriteLine("Wrong Input");
                Back();
            }

            Console.Clear();
            switch (menuKey)
            {
                case 1:
                    CountTasksOfCurrentUser();
                    break;
                case 2:
                    GetTasksOfCurrentUser();
                    break;
                case 3:
                    GetFinishedThisYearTasksOfCurrentUser();
                    break;
                case 4:
                    GetUsersOlderThenTenYearsOld();
                    break;
                case 5:
                    GetSortedUserByAscendingAndTasksByDescending();
                    break;
                case 6:
                    AnalyzeUserProjectsAndTasks();
                    break;
                case 7:
                    AnalyzeProjectTasksAndTeam();
                    break;
                case 8:
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Wrong menu option");
                    break;
            }

            Back();
        }


        public static void CountTasksOfCurrentUser()
        {
            Console.WriteLine("Write user ID: ");
            if (!int.TryParse(Console.ReadLine(), out var userId))
            {
                Console.WriteLine("Wrong input");
                return;
            }

            var query = Data.Projects
                .Where(p => p.AuthorId == userId)
                .ToDictionary(k => k.Id, v => v.Tasks.Count);
            foreach (var (key, value) in query)
            {
                Console.WriteLine($"ProjectId: {key}, Tasks: {value}");
            }

            Back();
        }


        public static void GetTasksOfCurrentUser()
        {
            Console.WriteLine("Write user ID: ");

            if (!int.TryParse(Console.ReadLine(), out var userId))
            {
                Console.WriteLine("Wrong input");
                return;
            }

            var query = Data.Tasks
                .Where(t => t.PerformerId == userId && t.Name.Length < 45)
                .ToList();

            foreach (var item in query)
            {
                Console.WriteLine($"\n\nName: {item.Name} \nDescription: {item.Description}");
            }

            Back();
        }


        public static void GetFinishedThisYearTasksOfCurrentUser()
        {
            Console.WriteLine("Write user ID: ");
            if (!int.TryParse(Console.ReadLine(), out var userId))
            {
                Console.WriteLine("Wrong input");
                return;
            }

            var query = Data.Tasks
                .Where(t => t.PerformerId == userId
                            && t.FinishedAt.HasValue
                            && t.FinishedAt.Value.Year == DateTime.Now.AddYears(-1).Year)
                .Select(task => new {task.Id, task.Name})
                .ToList();

            foreach (var item in query)
            {
                Console.WriteLine($"\n\nTask ID: {item.Id}\n Task Name: {item.Name}");
            }
        }

        public static void GetUsersOlderThenTenYearsOld()
        {
            var query = Data.Teams
                .Where(u => u.Users.Any(u => DateTime.Now.Year - u.BirthDay.Year > 10))
                .Select(t => new
                {
                    t.Id,
                    t.Name,
                    Users = t.Users
                        .Where(u => DateTime.Now.Year - u.BirthDay.Year > 10)
                        .OrderByDescending(t => t.RegisteredAt)
                        .ToList()
                });

            foreach (var item in query)
            {
                Console.WriteLine($"\n\nTeam ID: {item.Id}, Team Name: {item.Name}. \nUsers:");
                foreach (var user in item.Users)
                {
                    Console.WriteLine($"{user.FirstName} {user.LastName} ");
                }
            }
        }


        public static void GetSortedUserByAscendingAndTasksByDescending()
        {
            var query = Data.Users
                .Select(u => new User
                {
                    BirthDay = u.BirthDay,
                    Email = u.Email,
                    FirstName = u.FirstName,
                    Id = u.Id,
                    LastName = u.LastName,
                    RegisteredAt = u.RegisteredAt,
                    TeamId = u.TeamId,
                    Team = u.Team,
                    Tasks = u.Tasks.OrderByDescending(t => t.Name.Length).ToList()
                })
                .OrderBy(x => x.FirstName);


            foreach (var item in query)
            {
                Console.WriteLine($"\n\nUser First Name: {item.FirstName}. Tasks:");
                foreach (var task in item.Tasks)
                {
                    Console.WriteLine(task.Name);
                }
            }

            Back();
        }


        public static void AnalyzeUserProjectsAndTasks()
        {
            Console.WriteLine("Write user ID: ");
            if (!int.TryParse(Console.ReadLine(), out var userId))
            {
                Console.WriteLine("Wrong input");
                return;
            }


            var query = Data.Projects
                .Where(p => p.AuthorId == userId).OrderByDescending(project => project.CreatedAt).Take(1)
                .Select(p => new UserAdditionalInfo
                {
                    User = p.Author,
                    LastProject = p,
                    TotalTasksCount = p.Tasks.Count,
                    TotalUncompletedAndCanceledTasks = p.Tasks.Count(t => !t.FinishedAt.HasValue),
                    LongestTask = p.Tasks
                        .Where(t => t.FinishedAt.HasValue)
                        .OrderByDescending(t => t.FinishedAt.Value.Ticks - t.CreatedAt.Ticks)
                        .FirstOrDefault()
                })
                .FirstOrDefault();
            if (query == null)
            {
                Console.WriteLine("Not Found");
                return;
            }

            Console.WriteLine($"\n\nUser: {query.User.FirstName} {query.User.LastName}");
            Console.WriteLine($"Last Project: {query.LastProject?.Name}");
            Console.WriteLine($"Total tasks count: {query.TotalTasksCount}");
            Console.WriteLine($"Uncompleted and canceled tasks count: {query.TotalUncompletedAndCanceledTasks}");
            Console.WriteLine($"Longest task: {query.LongestTask?.Name}");


            Back();
        }


        public static void AnalyzeProjectTasksAndTeam()
        {
            Console.WriteLine("Write project ID: ");
            if (!int.TryParse(Console.ReadLine(), out var projectId))
            {
                Console.WriteLine("Wrong input");
                return;
            }

            var query = Data.Projects
                .Where(p => p.Id == projectId)
                .Select(p => new ProjectAdditionalInfo
                {
                    Project = p,
                    LongestTaskByDescription = p.Tasks
                        .OrderByDescending(t => t.Description.Length)
                        .FirstOrDefault(),
                    ShortestTaskByName = p.Tasks
                        .OrderBy(t => t.Name.Length)
                        .FirstOrDefault(),
                    TotalTeamCount = p.Description.Length > 20 ? p.Team.Users.Count :
                        p.Tasks.Count < 3 ? p.Team.Users.Count : 0
                });

            foreach (var item in query)
            {
                Console.WriteLine($"\n\nProject: {item.Project?.Name}");
                Console.WriteLine($"Longest task by description: {item.LongestTaskByDescription?.Description}");
                Console.WriteLine($"Shortest task by name: {item.ShortestTaskByName?.Name}");
                Console.WriteLine(
                    $"Total team count where project description length > 20 or tasks count < 3: {item.TotalTeamCount}");
            }

            Back();
        }


        public static async void InitializeData()
        {
            await Data.Initialize();
        }


        public static void Back()
        {
            Console.WriteLine("\nPress any key to go back...");
            Console.ReadKey();
            Start();
        }
    }
}