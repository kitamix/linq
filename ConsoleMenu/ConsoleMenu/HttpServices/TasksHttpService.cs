﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using ConsoleMenu.Models;
using Newtonsoft.Json;

namespace ConsoleMenu.HttpServices
{
    internal class TasksHttpService
    {
        private readonly HttpClient _httpClient = new();

        public TasksHttpService()
        {
            _httpClient.BaseAddress = new Uri($"{Settings.BaseAddress}/Tasks/");
        }

        public async System.Threading.Tasks.Task<List<Task>> GetAllTasks()
        {
            var content = await _httpClient.GetStringAsync("");
            return JsonConvert.DeserializeObject<List<Task>>(content);
        }

        public async System.Threading.Tasks.Task<Task> GetTaskById(int id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            return JsonConvert.DeserializeObject<Task>(content);
        }
    }
}