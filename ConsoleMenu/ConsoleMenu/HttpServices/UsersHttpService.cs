﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ConsoleMenu.Models;
using Newtonsoft.Json;

namespace ConsoleMenu.HttpServices
{
    internal class UsersHttpService
    {
        private readonly HttpClient _httpClient = new();

        public UsersHttpService()
        {
            _httpClient.BaseAddress = new Uri($"{Settings.BaseAddress}/Users/");
        }

        public async Task<List<User>> GetAllUsers()
        {
            var content = await _httpClient.GetStringAsync("");
            return JsonConvert.DeserializeObject<List<User>>(content);
        }

        public async Task<User> GetUserById(int id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            return JsonConvert.DeserializeObject<User>(content);
        }
    }
}