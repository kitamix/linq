﻿using System.Collections.Generic;
using System.Linq;
using ConsoleMenu.HttpServices;
using ConsoleMenu.Models;

namespace ConsoleMenu
{
    public static class Data
    {
        private static readonly ProjectsHttpService _projectsService = new();
        private static readonly TasksHttpService _tasksService = new();
        private static readonly TeamsHttpService _teamsService = new();
        private static readonly UsersHttpService _usersService = new();
        public static List<Project> Projects { get; set; }
        public static List<Task> Tasks { get; set; }
        public static List<Team> Teams { get; set; }
        public static List<User> Users { get; set; }

        public static async System.Threading.Tasks.Task Initialize()
        {
            var projects = await _projectsService.GetAllProjects();
            var tasks = await _tasksService.GetAllTasks();
            var teams = await _teamsService.GetAllTeams();
            var users = await _usersService.GetAllUsers();


            var connectedTeams = teams
                .GroupJoin(users,
                    t => t.Id,
                    u => u.TeamId,
                    (t, u) => new Team
                    {
                        Id = t.Id,
                        Name = t.Name,
                        CreatedAt = t.CreatedAt,
                        Users = u.ToList()
                    })
                .ToList();


            var connectedTasks = tasks
                .Join(users,
                    t => t.PerformerId,
                    u => u.Id,
                    (t, u) => new Task
                    {
                        Id = t.Id,
                        Name = t.Name,
                        PerformerId = t.PerformerId,
                        ProjectId = t.ProjectId,
                        State = t.State,
                        CreatedAt = t.CreatedAt,
                        FinishedAt = t.FinishedAt,
                        Description = t.Description,
                        Performer = u
                    }).ToList();


            var connectedProjects = projects
                .Join(users,
                    p => p.AuthorId,
                    u => u.Id,
                    (p, u) => new Project
                    {
                        AuthorId = p.AuthorId,
                        Deadline = p.Deadline,
                        Description = p.Description,
                        Id = p.Id,
                        Name = p.Name,
                        TeamId = p.TeamId,
                        CreatedAt = p.CreatedAt,
                        Author = u
                    })
                .Join(connectedTeams,
                    p => p.TeamId,
                    t => t.Id,
                    (p, t) => new Project
                    {
                        AuthorId = p.AuthorId,
                        Deadline = p.Deadline,
                        Description = p.Description,
                        Id = p.Id,
                        Name = p.Name,
                        TeamId = p.TeamId,
                        Author = p.Author,
                        CreatedAt = p.CreatedAt,
                        Team = t
                    })
                .GroupJoin(connectedTasks,
                    p => p.Id,
                    t => t.ProjectId,
                    (p, t) => new Project
                    {
                        AuthorId = p.AuthorId,
                        Deadline = p.Deadline,
                        Description = p.Description,
                        Id = p.Id,
                        Name = p.Name,
                        TeamId = p.TeamId,
                        Author = p.Author,
                        Team = p.Team,
                        CreatedAt = p.CreatedAt,
                        Tasks = t.ToList()
                    });

            var connectedUsers = users
                .Join(teams,
                    u => u.TeamId,
                    t => t.Id,
                    (u, t) => new User
                    {
                        BirthDay = u.BirthDay,
                        Email = u.Email,
                        FirstName = u.FirstName,
                        LastName = u.LastName,
                        Id = u.Id,
                        RegisteredAt = u.RegisteredAt,
                        TeamId = u.TeamId,
                        Team = t
                    })
                .Union(users.Where(u => u.TeamId == null))
                .GroupJoin(tasks,
                    u => u.Id,
                    t => t.PerformerId,
                    (u, t) => new User
                    {
                        BirthDay = u.BirthDay,
                        Email = u.Email,
                        FirstName = u.FirstName,
                        LastName = u.LastName,
                        Id = u.Id,
                        RegisteredAt = u.RegisteredAt,
                        TeamId = u.TeamId,
                        Team = u.Team,
                        Tasks = t.ToList()
                    });

            Tasks = connectedTasks.ToList();
            Projects = connectedProjects.ToList();
            Teams = connectedTeams.ToList();
            Users = connectedUsers.ToList();
        }
    }
}