﻿namespace ConsoleMenu.Models
{
    public class ProjectAdditionalInfo
    {
        public Project Project { get; set; }
        public Task? LongestTaskByDescription { get; set; }
        public Task? ShortestTaskByName { get; set; }
        public int TotalTeamCount { get; set; }
    }
}