﻿namespace ConsoleMenu.Models
{
    internal class UserAdditionalInfo
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int TotalTasksCount { get; set; }
        public int TotalUncompletedAndCanceledTasks { get; set; }
        public Task LongestTask { get; set; }
    }
}