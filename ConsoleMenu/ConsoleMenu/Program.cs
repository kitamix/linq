﻿namespace ConsoleMenu
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Menu.InitializeData();
            while (true)
            {
                Menu.Start();
            }
        }
    }
}